# frozen_string_literal: true

require 'digest'

require_relative 'community_processor'

require_relative '../../triage/rate_limit'

module Triage
  # Allows any community contributor to ask for a review.
  # The automation will pick a random MR coach and ask them to review the MR.
  class CommandMrRequestReview < CommunityProcessor
    include RateLimit

    REVIEWERS_REGEX = /@([^@[[:blank:]]]+)/.freeze

    react_to 'merge_request.note'
    define_command name: 'ready', aliases: %w[review request_review], args_regex: REVIEWERS_REGEX

    def applicable?
      valid_command?
    end

    def process
      comment = <<~MARKDOWN.chomp
        /label ~"#{Labels::WORKFLOW_READY_FOR_REVIEW_LABEL}"#{reviewer_username_action}
      MARKDOWN
      add_comment(comment, append_source_link: false)
    end

    def documentation
      <<~TEXT
      This allows any community contributor to ask for a review.

      The command can be used as `@gitlab-bot ready`, and is aliased to `@gitlab-bot review` and `@gitlab-bot request_review`.

      The command results in the `~"#{Labels::WORKFLOW_READY_FOR_REVIEW_LABEL}"` label being added to the merge request.

      If you know a relevant reviewer(s) (for example, someone that was involved in a related issue), you can also assign them directly with `@gitlab-bot ready @user1 @user2`.
      TEXT
    end

    private

    def reviewer_username_action
      return unless valid_reviewers.any?

      "\n/assign_reviewer #{reviewer_mentions}"
    end

    def valid_reviewers
      @valid_reviewers ||= command.args(event) & Triage.gitlab_org_group_member_usernames
    end

    def reviewer_mentions
      valid_reviewers.map { |reviewer| "@#{reviewer}" }.join(' ')
    end

    def cache_key
      @cache_key ||= Digest::MD5.hexdigest("request_review-commands-sent-#{event.event_actor_id}-#{event.noteable_path}")
    end

    def rate_limit_count
      event.by_team_member? ? 100 : 1
    end

    def rate_limit_period
      3600 # 1 hour
    end
  end
end
