# Running with a local GitLab instance

You can run `triage-ops` with a local GitLab instance (e.g GDK) and have it react to events coming from the local instance.

To load all prerequisites for your local GitLab instance, use the
[Triage Ops seed file](https://gitlab.com/gitlab-org/gitlab/-/blob/master/db/fixtures/development/33_triage_ops.rb)
by running this command in the GitLab Rails project directory:

```shell
cd <local-gitlab-instance-folder>
bundle exec rake db:seed_fu FILTER=33_triage_ops SEED_TRIAGE_OPS=true
```

This command:

- Generates a seed file.
- Creates and prints a new `GITLAB_API_TOKEN` and `GITLAB_WEBHOOK_TOKEN`.

# Running locally

```shell
GITLAB_API_ENDPOINT=http://127.0.0.1:3000/api/v4 \
  GITLAB_WEBHOOK_TOKEN=triage-ops-webhook-token \
  GITLAB_API_TOKEN=<API_TOKEN_PRINTED_IN_RAKE_TASK_ABOVE>
  SLACK_WEBHOOK_URL=https://example.org \
  DRY_RUN=1 \
  bundle exec puma -b tcp://0.0.0.0:8091
```

NOTE:
- `DRY_RUN=1` allows you to test triage without making API calls to GitLab.
- Remove or set `DRY_RUN=0` if/when you want API calls to be made.

You can now test the endpoint:

```shell
curl -X POST 0.0.0.0:8091 -H "Content-Type: application/json" -H "X-Gitlab-Token: triage-ops-webhook-token" -d @spec/fixtures/reactive/gitlab_test_note.json
```

# Running in docker

```shell
# Docker command
# 
# Note: You probably should not use docker desktop at GitLab.
# See https://about.gitlab.com/handbook/tools-and-tips/mac/#docker-desktop for more info
# 
# Also, host.docker.internal is for macOS hosts only. For linux, you might want to use 172.17.0.1
#
# (source: https://stackoverflow.com/questions/48546124/what-is-linux-equivalent-of-host-docker-internal)
{
  docker rm `docker ps -aq`
  docker build --file=Dockerfile.rack -t triage-ops .
  docker run -itp 8091:8080 \
    -e GITLAB_API_ENDPOINT=http://host.docker.internal:3000/api/v4 \
    -e GITLAB_WEBHOOK_TOKEN=triage-ops-webhook-token \
    -e GITLAB_API_TOKEN=<API_TOKEN_PRINTED_IN_RAKE_TASK_ABOVE> \
    -e SLACK_WEBHOOK_URL=https://example.org \
    -e DRY_RUN=1 \
    triage-ops
}
```

NOTE:
- You can use `nerdctl` command to run triage-ops with Docker by setting `GITLAB_API_ENDPOINT=http://192.168.5.2:3000/api/v4`.
- The IP 192.168.5.2 comes from [the Lima docs](https://github.com/lima-vm/lima/blob/master/docs/network.md#host-ip-19216852).
- `DRY_RUN=1` allows you to test triage without making API calls to GitLab.
- Remove or set `DRY_RUN=0` if/when you want API calls to be made.

TODO:
- [ ] Stub Slack webhook URL
