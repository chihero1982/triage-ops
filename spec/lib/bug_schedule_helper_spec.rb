# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/bug_schedule_helper'

RSpec.describe BugScheduleHelper do
  let(:bug_schedule_helper) do
    Class.new do
      attr_reader :milestone

      include BugScheduleHelper

      def initialize(milestone)
        @milestone = milestone
      end

      def label_names
        ['group::triageops']
      end
    end
  end

  let(:today) { '2022-07-07' }
  let(:five_days_ago) { '2022-07-02' }
  let(:five_days_later) { '2022-07-12' }

  let(:milestone) do
    Gitlab::Triage::Resource::Milestone.new({ title: "15.3", due_date: five_days_later, start_date: five_days_ago })
  end

  subject { bug_schedule_helper.new(milestone) }

  around do |example|
    Timecop.freeze(today) { example.run }
  end

  describe '#milestone_matches_slo_target?' do
    let(:one_year_ago) { "2021-01-10" }
    let(:one_year_later) { "2023-07-01" }

    it 'returns false when issue does not belong to any milestone' do
      expect(subject.milestone_matches_slo_target?(nil, "severity::1")).to be(false)
    end

    context 'when milestone is expired' do
      let(:expired_milestone) do
        Gitlab::Triage::Resource::Milestone.new({ due_date: one_year_ago, start_date: one_year_ago })
      end

      it 'returns false' do
        expect(subject.milestone_matches_slo_target?(expired_milestone, "severity::1")).to be(false)
      end
    end

    context 'when milestone start date is later than the slo target date' do
      let(:milestone_too_far) do
        Gitlab::Triage::Resource::Milestone.new({ due_date: one_year_later, start_date: one_year_later })
      end

      it 'returns false' do
        expect(subject.milestone_matches_slo_target?(milestone_too_far, "severity::1")).to be(false)
      end
    end

    context 'when milestone is not expired and has a start date earlier than slo target date' do
      it 'returns true' do
        expect(subject.milestone_matches_slo_target?(milestone, "severity::1")).to be(true)
      end
    end
  end

  describe '#mention_team_members_for_slo_target_mismatch' do
    context "when none of the team members is found" do
      before do
        allow(subject).to receive(:current_group_em).and_return(nil)
        allow(subject).to receive(:current_group_pm).and_return(nil)
        allow(subject).to receive(:current_group_set).and_return(nil)
        allow(subject).to receive(:current_group_pd).and_return(nil)
      end

      it "returns the group label instead" do
        expect(subject.mention_team_members_for_slo_target_mismatch).to eq(%(~"group::triageops"))
      end
    end

    context "when some of the team members are found" do
      before do
        allow(subject).to receive(:current_group_em).and_return('@em')
        allow(subject).to receive(:current_group_pm).and_return(nil)
        allow(subject).to receive(:current_group_set).and_return('@set')
        allow(subject).to receive(:current_group_pd).and_return('@pd')
      end

      it "returns string interpolted with every available username" do
        expect(subject.mention_team_members_for_slo_target_mismatch).to eq('@em @set @pd')
      end
    end
  end

  describe '#set_milestone_with_slo_target' do
    let(:milestone_valid_for_slo) do
      Gitlab::Triage::Resource::Milestone.new({ title: '17.0' })
    end

    before do
      allow_any_instance_of(VersionedMilestone).to receive(:find_milestone_for_date).with(Date.parse(today) + 30).and_return(milestone_valid_for_slo)
    end

    it 'returns milestone matching the given date' do
      expect(subject.set_milestone_with_slo_target("severity::1")).to eq(%(/milestone %"17.0"))
    end
  end
end
