# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/automated_review_request_generic'

RSpec.describe Triage::AutomatedReviewRequestGeneric do
  let(:group) { 'code review' }
  let(:distribution_project_id) { 42 }

  include_context 'with event', 'Triage::MergeRequestEvent' do
    let(:label_names) { ['foo', "group::#{group}"] }
    let(:added_label_names) { [Labels::WORKFLOW_READY_FOR_REVIEW_LABEL] }
  end

  subject { described_class.new(event) }

  before do
    allow(WwwGitLabCom).to receive(:distribution_projects).and_return([distribution_project_id])
  end

  include_examples 'registers listeners', ["merge_request.update"]

  describe '#applicable?' do
    it_behaves_like 'community contribution open resource #applicable?'

    context 'when "workflow::ready for review" is not added' do
      let(:added_label_names) { ['bug'] }

      include_examples 'event is not applicable'
    end

    context 'when project_id is a distribution project' do
      let(:project_id) { distribution_project_id }

      include_examples 'event is not applicable'
    end
  end

  describe '#process' do
    let(:coach_username) { '@coach_username' }
    let(:current_reviewers) { [] }
    let(:merge_request) do
      {
        project_id: event.project_id,
        iid: event.iid,
        reviewers: current_reviewers
      }
    end

    before do
      stub_api_request(path: "/projects/#{event.project_id}/merge_requests/#{event.iid}", response_body: merge_request)
      allow(subject).to receive(:select_random_merge_request_coach).with(group: group).and_return(coach_username)
    end

    shared_examples 'process merge request review request' do
      it 'posts a comment mentioning an MR coach to request a review' do
        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    context 'when MR does not have any reviewer set' do
      it_behaves_like 'process merge request review request' do
        let(:body) do
          add_automation_suffix('community/automated_review_request_generic.rb') do
            <<~MARKDOWN.chomp
              `#{coach_username}`, this ~"Community contribution" is ready for review.

              /assign_reviewer #{coach_username}

              - Do you have capacity and domain expertise to review this? We are mindful of your time, so if you are not
                able to take this on, please re-assign to one or more other reviewers.
              - Add the ~"workflow::in dev" label if the merge request needs action from the author.
              /unlabel ~"#{Labels::AUTOMATION_AUTHOR_REMINDED_LABEL}"
            MARKDOWN
          end
        end
      end
    end

    context 'when MR already has reviewers set' do
      let(:current_reviewers) do
        [
          {
            id: 1,
            name: "John Doe",
            username: "john_doe"
          },
          {
            id: 2,
            name: "Jane Die",
            username: "jane_die"
          }
        ]
      end

      it_behaves_like 'process merge request review request' do
        let(:body) do
          add_automation_suffix('community/automated_review_request_generic.rb') do
            <<~MARKDOWN.chomp
              `@john_doe` `@jane_die`, this ~"Community contribution" is ready for review.

              - Do you have capacity and domain expertise to review this? We are mindful of your time, so if you are not
                able to take this on, please re-assign to one or more other reviewers.
              - Add the ~"workflow::in dev" label if the merge request needs action from the author.
              /unlabel ~"#{Labels::AUTOMATION_AUTHOR_REMINDED_LABEL}"
            MARKDOWN
          end
        end
      end
    end
  end
end
