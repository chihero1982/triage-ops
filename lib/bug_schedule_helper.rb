# frozen_string_literal: true

require_relative 'devops_labels'
require_relative 'versioned_milestone'
require_relative 'constants/slo_targets'
require_relative 'shared/current_group_helper'

module BugScheduleHelper
  include DevopsLabels::Context
  include CurrentGroupHelper

  def milestone_matches_slo_target?(milestone, severity)
    !!milestone&.start_date && milestone.start_date < slo_target_date(severity) && !milestone.expired?
  end

  def mention_team_members_for_slo_target_mismatch
    @redact_confidentials = false
    members_to_ping = [current_group_em, current_group_pm, current_group_set, current_group_pd].compact
    if members_to_ping.empty?
      %(~"#{current_group_label}")
    else
      members_to_ping.join(' ')
    end
  end

  def set_milestone_with_slo_target(severity)
    milestone = VersionedMilestone.new(self).find_milestone_for_date(slo_target_date(severity))
    return unless milestone

    %(/milestone %"#{milestone.title}")
  end

  def slo_target_date(severity)
    today + slo_target(severity)
  end

  def slo_target(severity, category = "default")
    # TODO: based on # https://about.gitlab.com/handbook/engineering/quality/issue-triage
    # expand this method to include severity slo targets for special bug cateogies
    SloTargets::DEFAULT_SEVERITY_SLO_TARGETS[severity]
  end

  def today
    @today ||= Date.today
  end
end
